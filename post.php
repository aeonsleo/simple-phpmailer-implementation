<?php
    require_once "vendor/autoload.php";
    ini_set('display_errors', 1);

    if(isset($_POST['body'])) {
        //PHPMailer Object
        // $mail = new PHPMailer;
        $mail = new PHPMailer\PHPMailer\PHPMailer();

        //Enable SMTP debugging.
        $mail->SMTPDebug = 3;
        //Set PHPMailer to use SMTP.
        $mail->isSMTP();
        //Set SMTP host name
        $mail->Host = "smtp.gmail.com";
        //Set this to true if SMTP host requires authentication to send email
        $mail->SMTPAuth = true;
        //Provide username and password
        $mail->Username = "from@email.com";
        $mail->Password = "password";
        //If SMTP requires TLS encryption then set it
        $mail->SMTPSecure = "tls";
        //Set TCP port to connect to
        $mail->Port = 587;


        //From email address and name
        $mail->From = "from@email.com";
        $mail->FromName = "Your Name";

        //To address and name
        $mail->addAddress($_POST['email'], $_POST['name']);
        // $mail->addAddress("recepient1@example.com"); //Recipient name is optional

        //Address to which recipient will reply
        $mail->addReplyTo("reply@yourdomain.com", "Reply");

        //CC and BCC
        // $mail->addCC("cc@example.com");
        // $mail->addBCC("bcc@example.com");

        //Send HTML or Plain Text email
        $mail->isHTML(true);

        $mail->Subject = "This is test from smtp Gmail account";
        $mail->Body = $_POST['body'];
        // $mail->AltBody = "This is the plain text version of the email content";

        if(!$mail->send())
        {
            echo "Mailer Error: " . $mail->ErrorInfo;
        }
        else
        {
            echo "Message has been sent successfully";
        }
    }
